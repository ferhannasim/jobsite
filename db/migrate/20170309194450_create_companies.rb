class CreateCompanies < ActiveRecord::Migration[5.0]
  def change
    create_table :companies do |t|
      t.string :name
      t.integer :number_of_employee
      t.string :location
      t.string :website
      t.string :jobs
      t.integer :phone

      t.timestamps
    end
  end
end
