class ChangePhoneTypeInCompany < ActiveRecord::Migration[5.0]
  def up
    change_column :companies, :phone, :string
  end
  
  def down
    change_column :companies, :phone, :integer
  end
end
