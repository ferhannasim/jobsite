class CreatePostjobs < ActiveRecord::Migration[5.0]
  def change
    create_table :postjobs do |t|
      t.string :job_title
      t.text :job_description
      t.string :company_name

      t.timestamps
    end
  end
end
  