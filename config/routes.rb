Rails.application.routes.draw do
  
  root to: 'welcome#new'
  default_url_options :host => "http://localhost:3000"
  devise_for :users, path_names: { sign_in: "login", sign_out: "logout", :sign_up => "register" }
  match '/users/:id/destroy', to: 'users#destroy', via: [:get, :patch], as: 'signout'
  
  resources :postjobs
  resources :welcome
  resources :jobseeker_dashboard,only: [:index] do
    member do
      get :apply_form
      post :submit_form
    end
  end
  resources :employer_dashboard, only: [:index] do
    collection do
      get :new_company
      post :create_company
      get :edit_company
      put :update_company
    end
  end

end
