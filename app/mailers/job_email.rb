class JobEmail < ApplicationMailer
  default from: 'notifications@example.com'
  
  def welcome_email(email , body, subject, file)
    attachments.inline['resume.pdf'] = File.read(file)
    mail(to: email, subject:subject, body: body )
  end
end