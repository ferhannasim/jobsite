class ApplicationMailer < ActionMailer::Base
  default from: "no-reply@ferhanmail.com"
  layout 'mailer'
end