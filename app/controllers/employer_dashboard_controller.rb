class EmployerDashboardController < ApplicationController
  
  def index
  
  end
  
  def new_company
    @company=Company.new
  end
  
  def create_company
    @company = Company.new(company_params)
    @company.save
    redirect_to new_postjob_url
  end
  
  def show_company
    @company = Company.find(params[:id])
  end

  def edit_company
    @company = current_user.company
  end
  def update_company
    @company = current_user.company
    @company.update(company_params)
    redirect_to employer_dashboard_index_path
  end
  private
  def company_params
    params.require(:company).permit(:name, :number_of_employee,:location, :website, :phone, :jobs,:user_id)
  
  end
end