class ApplicationController < ActionController::Base
  include AppConstants
  helper_method :employer?, :jobseeker?,:logged_in?
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?


  def employer?
    self.try(:account_type) == EMPLOYER
  end

  def jobseeker?
    self.try(:account_type) == JOBSEEKR
  end
  def logged_in?
    current_user
  end
  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:account_type])
  end
  
end
