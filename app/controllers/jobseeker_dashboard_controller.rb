class JobseekerDashboardController < ApplicationController
  
  def index
    @jobs=Postjob.all
  end
  
  def apply_form
    @job_id=params[:id]
  end
  
  def submit_form
    @file  = params[:apply][:uploaded_file].path
    job = Postjob.find(params[:apply][:job_id])
    email = job.company.user.email
    @subject = "Job Applicant CV"
    @body = "My name is #{params[:apply][:applicant_name]}.This is my email address #{params[:apply][:email]}.My current salary is #{params[:apply][:current_salary]}.
             and my expected salary is #{params[:apply][:expected_salary]}.Thank you "
    JobEmail.welcome_email(email , @body, @subject, @file).deliver_now
    redirect_to jobseeker_dashboard_index_path, notice: 'you had applied sucessfully'
  end
end
