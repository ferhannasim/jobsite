class PostjobsController < ApplicationController
  
  def new
    if current_user.company.present?
      @company_id=current_user.company.id
    else
      redirect_to new_company_employer_dashboard_index_path  , notice: "please Register your company first"
    end
    
  end
  
  def create
    
    @job = Postjob.new(job_params)
    @job.save
    redirect_to postjobs_path
  end
  
  def index
    @jobs = Postjob.all
  
  end
  
  def show
    
    @job = Postjob.find(params[:id])
  end
  
  def edit
    @job = Postjob.find(params[:id])
  end
  
  def update
    @job = Postjob.find(params[:id])
    @job.update(job_params)
    redirect_to postjobs_path
  end
  
  def destroy
    @job = Postjob.find(params[:id])
    @job.destroy
    redirect_to postjobs_path
  end
  
  private
  def job_params
    params.require(:job).permit(:job_title, :job_description, :company_name, :skills, :qualification, :experience, :salary, :company_id)
  end
end
