class WelcomeController < ApplicationController
  def index
  
  end

  def new
    if logged_in?
      if current_user.account_type == "EMPLOYER"
        redirect_to employer_dashboard_index_path
      else
        redirect_to jobseeker_dashboard_index_path
      end
    else
      redirect_to new_user_session_url
    end
  end
end
