class Company < ApplicationRecord
  belongs_to :user
  has_many :post_jobs

  validates :user_id, presence: true

end